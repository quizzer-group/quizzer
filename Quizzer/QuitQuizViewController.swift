//
//  QuitQuizViewController.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-11-29.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class QuitQuizViewController: UIViewController {

    var image = UIImage()
    var username: String = ""
    
    //MARK: - View Loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()

    }
    //MARK: - Animations
    @IBAction func closeModal(_ sender: Any) {
        removeAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.view.removeFromSuperview()
            }
        })
    }
    
    //MARK: - Preapare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ViewController {
            let vc = segue.destination as? ViewController
            vc?.image = self.image
            vc?.username = self.username
        }
    }
}
