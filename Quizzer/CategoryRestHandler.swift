//
//  CategoryRestHandler.swift
//  Quizzer
//
//  Created by Nils Müller on 22.11.19.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation

class CategoryRestHandler {
    //MARK: - Fetch Categories
    static func fetchCategories(completion: ((Bool) -> Void)?) {
        
        // fetch category information from opentdb
        guard let url = URL(string: "https://opentdb.com/api_category.php") else {
            // If the url object is nil we'll send false through the callback and end the function.
            completion?(false)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let task = session.dataTask(with: url) { (responseData, response, responseError) in
            let decoder = JSONDecoder()
            if let data = responseData, let categories = try? decoder.decode(CategoryResponse.self, from: data) {
                CategoryDataHandler.instance.setCategories(categories.trivia_categories)
                completion?(true)
                return
            }
            
            // A callback always needs to be called so the the class calling this function gets a response
            completion?(false)
        }
        
        task.resume()
    }
}
