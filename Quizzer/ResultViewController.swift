//
//  ResultViewController.swift
//  Quizzer
//
//  Created by William Söder on 2019-11-15.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBAction func tryAgain(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "tryAgain", sender: self)
            }
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    var image = UIImage()
    var result = [Bool]()
    var usersAnswer = [String?]()
    var quiz = [Questions]()
    var stars = [UIImage]()
    var starsToAnimate = [UIImageView]()
    var arrows = [UIImage]()
    var arrowsToAnimate = [UIImageView]()
    var username: String = ""

    //MARK: - View Loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        userImage.layer.cornerRadius = userImage.frame.width/2
        tryAgainButton.layer.cornerRadius = 10
        homeButton.layer.cornerRadius = 10
        
        userImage.image = image
        background.image = UIImage(named: "background_3")
        
        printMessage()
        createImageArrays()
        createStarsView(starViewsToCreate: stars)
        createArrowsView(arrowViewsToCreate: arrows)
        animateViews(viewsToAnimate: starsToAnimate)
        animateViews(viewsToAnimate: arrowsToAnimate)
        animateArrows(viewsToAnimate: arrowsToAnimate)
    }
    
    //MARK: - Message to user
    func printMessage(){
        var sumOfCorrectAnswers = 0
        for answer in result{
            if answer == true{
                sumOfCorrectAnswers += 1
            }
        }
        score.text = "\(sumOfCorrectAnswers)/\(result.count)"
        
        if sumOfCorrectAnswers <= 5 {
            message.text = "Better luck next time \(username)! "
        }
        else if sumOfCorrectAnswers < 10{
            message.text = "Good job \(username)!"
        }
        else{
            message.text = "Perfect score \(username)!"
        }
    }
    
    //MARK: - Tap Function
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        let tagNumber: Int = gesture.view!.tag
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QuestionDetails") as! QuestionDetailsViewController

        vc.question = quiz[tagNumber].question
        vc.correctAnswer = quiz[tagNumber].correct_answer.htmlAttributedString!.string
        vc.usersAnswer = usersAnswer[tagNumber]!
        vc.questionNr = "Question #\(tagNumber+1)"
        
        self.addChild(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
        
    }
    
    //MARK: Animated Views
    func createImageArrays(){
        for answer in result{
            if (answer == true){
                stars.append(#imageLiteral(resourceName: "greenStar.png"))
                arrows.append(#imageLiteral(resourceName: "greenArrow.png"))
            }
            else{
                stars.append(#imageLiteral(resourceName: "redStar.png"))
                arrows.append(#imageLiteral(resourceName: "redArrow.png"))
            }
        }
    }
    
    func createArrowsView(arrowViewsToCreate: [UIImage]){
        var xCordinate: Int = 2
        let width = Int(self.view.layer.frame.width / 10)
        
        for arrowImage in arrowViewsToCreate {
            
            let arrowView = UIImageView(image: arrowImage)
            arrowView.frame = CGRect(x: xCordinate, y: 10, width: width, height: 20)
            
            self.view.addSubview(arrowView)
            arrowsToAnimate.append(arrowView)
            xCordinate += width
        }
    }

    func createStarsView(starViewsToCreate: [UIImage]){
        var xCordinate: Int = 2
        let width = Int(self.view.layer.frame.width / 10)
        var tagNumber: Int = 0
        
        for starImage in starViewsToCreate{
            let imageView = UIImageView(image: starImage)
            imageView.frame = CGRect(x: xCordinate, y: 30, width: width, height: 30)
            imageView.tag = tagNumber
            
            
            // create tap gesture recognizer
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ResultViewController.imageTapped(gesture:)))
            // add it to the image view;
            imageView.addGestureRecognizer(tapGesture)
            // make sure imageView can be interacted with by user
            imageView.isUserInteractionEnabled = true
            
            self.view.addSubview(imageView)
            starsToAnimate.append(imageView)
            
            xCordinate += width
            tagNumber += 1
        }
        
    }
    
    func animateViews(viewsToAnimate: [UIImageView]){
        var time: Int = 0
        
        for view in viewsToAnimate {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(time)) {
                UIView.animate(withDuration: 0.5){
                    view.frame.origin.y += 400
                }
            }
            time += 250
        }
        
    }
    
    func animateArrows(viewsToAnimate: [UIImageView]){
        for view in viewsToAnimate {
            UIView.animate(withDuration: 0.5, delay: 3.5, options: [.repeat,.autoreverse], animations: {
                view.alpha = 0.0
            }, completion: nil)
        }
    }
    
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is QuizViewController
        {
            let vc = segue.destination as? QuizViewController
            vc?.quiz = quiz
            vc?.image = self.image
            vc?.username = self.username
        }
        if segue.destination is ViewController{
            let vc = segue.destination as? ViewController
            vc?.image = self.image
            vc?.username = self.username
        }
        
    }
}

