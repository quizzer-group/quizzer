//
//  MusicPlayer.swift
//  Quizzer
//
//  Created by Voltair on 2019-11-21.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation
import AVFoundation

class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer? //Audio player for background music
    var audioPlayer2: AVAudioPlayer? //Audio player for sound effects
    
    //MARK: - Music Control
    func startBackgroundMusic(_ backgroundMusicFileName: String) {
        if let bundle = Bundle.main.path(forResource: backgroundMusicFileName, ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
    
    //MARK: Sound Effects
    func soundEffect(soundEffect: String){
        if let sound = Bundle.main.path(forResource: soundEffect, ofType: "mp3"){
            let soundURL = NSURL(fileURLWithPath: sound)
            do{
                audioPlayer2 = try AVAudioPlayer(contentsOf: soundURL as URL)
                guard let audioPlayer2 = audioPlayer2 else {return}
                audioPlayer2.play()
            } catch {
                print(error)
            }
        }
        
        
    }

}
