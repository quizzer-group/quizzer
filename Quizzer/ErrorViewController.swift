//
//  ErrorViewController.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-12-02.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    var errorTitle: String = ""
    var errorMessage: String = ""
    var username: String = ""
    var type: String = ""
    var image = UIImage()
    
    //MARK: - View Loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        titleLabel.text = errorTitle
        infoLabel.text = errorMessage
        self.showAnimate()
    }
    
    //MARK: - Animations
    @IBAction func closeModal(_ sender: Any) {
        removeAnimate()
        if(type == "categories") {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "errorOccurred", sender: self)
            }
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.view.removeFromSuperview()
            }
        })
    }
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ProfilePictureController{
            destination.profileImage = self.image
            destination.userName = self.username
        }
    }
}
