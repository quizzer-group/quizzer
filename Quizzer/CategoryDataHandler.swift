//
//  CategoryDataHandler.swift
//  Quizzer
//
//  Created by Nils Müller on 22.11.19.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation
import UIKit

class CategoryDataHandler {
    static let instance = CategoryDataHandler()
    
    private var categories: [Category]?
    //MARK: - Handle Caegories
    func getCategories() -> [Category]? {
        return categories
    }
    
    func setCategories(_ categories: [TriviaCategory]) {
        self.categories = categories.map({(category) -> Category in
            let name = category.name.split(separator: ":")
            var iconName = ""
            
            for i in name{
                iconName += i
            }
            return Category(image: UIImage(named: iconName), title: category.name, id: category.id)
        })
    }
}
