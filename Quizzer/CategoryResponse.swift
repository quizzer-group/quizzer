//
//  CategoryResponse.swift
//  Quizzer
//
//  Created by Nils Müller on 22.11.19.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation

//MARK: - Category Response
struct CategoryResponse: Decodable {
    let trivia_categories: [TriviaCategory]
}

//MAKR: - Categories
struct TriviaCategory: Decodable {
    let id: Int
    let name: String
}
