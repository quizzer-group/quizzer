//
//  ViewController.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-11-08.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    
    var username: String = ""
    var categories: [Category] = []
    var questions: [Questions] = []
    var currentCategoryID = 9
    var image = UIImage()
    var allButtons:[UIButton] = []
    
    //MARK: - View Loaded
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        loadingView.layer.cornerRadius = 10
        
        showLoadingAnimation()
        
        getCategories() {
            self.hideLoadingAnimation()
        }
        
        setupPickerView()
        setProfileInformation()
        
        background.image = UIImage(named: "background_3")
    }
    
    override func viewDidLayoutSubviews() {
        for subview in pickerView.subviews{
            if subview.frame.origin.y != 0{
                subview.isHidden = true
            }
        }
    }
    
    //MARK: - Setup function
    private func showLoadingAnimation() {
        loadingView.isHidden = false
        allButtons = getAllButtons()
        disableAllButtons()
    }
    
    private func hideLoadingAnimation() {
        self.loadingView.isHidden = true
        self.enableAllButtons()
    }
    
    private func setupPickerView() {
        pickerView.transform = CGAffineTransform(rotationAngle: 90 * (.pi/180))
        pickerView.frame = CGRect(x: -100, y: 400, width: view.frame.width + 200, height: 100)
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.selectRow(categories.count / 2, inComponent: 0, animated: true)
    }
    
    private func setProfileInformation() {
        profileButton.layer.masksToBounds = true
        profileButton.layer.cornerRadius = profileButton.frame.width/2
        profileButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        profileButton.setImage(image, for: .normal)
        
        self.usernameLabel.text = username
    }
    
    //MARK: - Categories Functions
    
    fileprivate func getCategories(completion: @escaping () -> ()) {
        CategoryRestHandler.fetchCategories { (success) in
            if success, let result = CategoryDataHandler.instance.getCategories() {
                DispatchQueue.main.async {
                    self.categories = result
                    self.pickerView.reloadAllComponents()
                    completion()
                }
                
            } else {
                self.showError(title: "An error occurred", message: "Unfortunatly an error occured when loading the categories. Please try again.", type: "categories")
            }
        }
    }
    
    //MARK: - Profile Functions
    @IBAction func ProfileButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ChangePicture", sender: self)
        }
    }
    
    //MARK: - Button Functions
    @IBAction func EasyButtonPressed(_ sender: Any) {
        getQuestions(category: currentCategoryID, difficulty: "easy"){
            DispatchQueue.main.async {
                self.enableAllButtons()
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: "startQuiz", sender: self)
            }
        }
        disableAllButtons()
        loadingView.isHidden = false
    }
    @IBAction func MediumButtonPressed(_ sender: Any) {
        getQuestions(category: currentCategoryID, difficulty: "medium"){
            DispatchQueue.main.async {
                self.enableAllButtons()
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: "startQuiz", sender: self)
            }
        }
        disableAllButtons()
        loadingView.isHidden = false
    }
    @IBAction func HardButtonPressed(_ sender: Any) {
        getQuestions(category: currentCategoryID, difficulty: "hard"){
            DispatchQueue.main.async {
                self.enableAllButtons()
                self.loadingView.isHidden = true
                self.performSegue(withIdentifier: "startQuiz", sender: self)
            }
        }
        disableAllButtons()
        loadingView.isHidden = false
    }
    
    func getAllButtons() -> [UIButton] {
        var btnArray:[UIButton] = []
        for subview in view.subviews {
            if(subview is UIButton) {
                btnArray.append(subview as! UIButton)
            }
        }
        for btn in btnArray{
            if (["Easy", "Medium", "Hard", "Resume"].contains(btn.titleLabel?.text)) {
                btn.layer.cornerRadius = 10
            }
        }
        return btnArray
    }
    
    func disableAllButtons(){
        for btn in allButtons{
            btn.isEnabled = false
        }
    }
    
    func enableAllButtons(){
        for btn in allButtons{
            btn.isEnabled = true
        }
    }
    
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.destination is QuizViewController
        {
            let destination = segue.destination as? QuizViewController
            destination?.quiz = questions
            destination?.image = self.image
            destination?.username = self.usernameLabel.text ?? ""
        }
        if let destination = segue.destination as? ProfilePictureController{
            destination.profileImage = self.image
            destination.userName = self.usernameLabel.text ?? ""
        }
    }
}

//MARK: - Class Extensions
extension ViewController: UIPickerViewDataSource {
    // Like number of columns
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
}
extension ViewController: UIPickerViewDelegate {
    //MARK: - Picker View Functions
    private func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> Category? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentCategoryID = categories[row].id
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        let image = UIImageView(image: categories[row].image)
        image.frame = CGRect(x: 15, y:0, width: 70, height: 70)
        view.addSubview(image)
        
        let time = UILabel(frame: CGRect(x: 0, y: 85, width: view.frame.width, height: 60))
        time.text = categories[row].title
        time.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.thin)
        time.lineBreakMode = .byWordWrapping;
        time.numberOfLines = 0;
        time.textAlignment = .center
        view.addSubview(time)
        
        view.transform = CGAffineTransform(rotationAngle: -90 * (.pi/180))
        
        return view
    }
    
    //MARK: - Question Functions
    func getQuestions(category: Int, difficulty: String, completion: @escaping () -> ()){
        let quizRequest = QuizRequest(category: category, difficulty: difficulty)
        quizRequest.getQuestions { [weak self] result in
            switch result {
            case .failure(let error):
                self!.showError(title: "An error occurred", message: "Unfortunately, an error occured when loading the quiz. Please try again.", type: "questions")
                print(error)
            case .success(let questions):
                self?.questions = questions
                completion()
            }
        }
    }
    
    //MARK: - Error Functions
    func showError(title: String, message: String, type: String){
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Error") as! ErrorViewController
            vc.errorTitle = title
            vc.errorMessage = message
            vc.type = type
            vc.username = self.username
            vc.image = self.image
            self.addChild(vc)
            vc.view.frame = self.view.frame
            self.view.addSubview(vc.view)
            vc.didMove(toParent: self)
            self.enableAllButtons()
            self.loadingView.isHidden = true
        }
    }
}

