//
//  QuizRequest.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-11-11.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation

//MARK: - Errors
enum QuestionError: Error {
    case noDataAvailable
    case canNotProcessData
}

//MARK: - Quiz Request
struct QuizRequest {
    let resourceURL: URL
    
    //MARK: - Initialization
    init(category: Int, difficulty: String)
    {
        let resourceString = "https://opentdb.com/api.php?amount=10&category=\(category)&difficulty=\(difficulty)"
        
        guard let resourceURL = URL(string: resourceString) else {
            fatalError()
            // Handle error properly
        }
        self.resourceURL = resourceURL
    }
    
    //MARK: - Get Questions
    func getQuestions (completion: @escaping(Result<[Questions], QuestionError>) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: resourceURL) {
            data, _, _ in
            guard let jsonData = data else {
                completion(.failure(.noDataAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let quizResponse = try decoder.decode(QuizResponse.self, from: jsonData)
                let questions = quizResponse.results
                let response_code = quizResponse.response_code
                if(response_code == 0){
                    completion(.success(questions))
                }
                else{
                    completion(.failure(.noDataAvailable))
                }
            }catch{
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
}
