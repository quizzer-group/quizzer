//
//  QuestionDetailsViewController.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-11-20.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class QuestionDetailsViewController: UIViewController {

    @IBAction func backToResults(_ sender: Any) {
        removeAnimate()
    }
    @IBOutlet weak var questionNrLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var correctAnswerLabel: UILabel!
    @IBOutlet weak var usersAnswerLabel: UILabel!
    var question = ""
    var correctAnswer = ""
    var usersAnswer = ""
    var questionNr = ""
    
    //MARK: - View Loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionLabel.text = question.htmlAttributedString?.string
        correctAnswerLabel.text = correctAnswer
        usersAnswerLabel.text = usersAnswer
        questionNrLabel.text = questionNr
        
        if correctAnswer == usersAnswer{
            usersAnswerLabel.backgroundColor = UIColor.systemGreen
        }
        else{
            usersAnswerLabel.backgroundColor = UIColor.red
        }
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()

    }

    //MARK: - Animations
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        
        }, completion:{(finished : Bool)  in
            if (finished) {
                self.view.removeFromSuperview()
            }
        });
    }
    
    //MARK: - Button Border
    func setButtonBorders() {
        for subview in view.subviews {
            if(subview is UIButton) {
                subview.layer.cornerRadius = 10
            }
        }
    }
}
