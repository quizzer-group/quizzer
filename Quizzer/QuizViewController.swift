//
//  QuizViewController.swift
//  Quizzer
//
//  Created by William Söder on 2019-11-11.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {
    
    var image = UIImage()
    
    var quiz = [Questions]()
    var amountOfQuestions:Int = 0
    var currentQuestionIndex:Int = 0
    var correctlyAnswered:[Bool] = []
    var usersAnswer:[String?] = []
    var username: String = ""
    var oldQuestion: String = ""
    
    var optionButtons:[UIButton] = []
    
    @IBOutlet weak var questionNrLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var background: UIImageView!

    //MARK: - View Loaded
    override func viewDidLoad() {
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        MusicPlayer.shared.startBackgroundMusic("song2")
        super.viewDidLoad()
        optionButtons = getButtons()
        setLabelBorders()
        background.image = UIImage(named: "background_3")
        
        for _ in quiz {
            amountOfQuestions += 1
            correctlyAnswered.append(false)
        }
        
        setQuestion(questionIndex: currentQuestionIndex)
    }
    
    //MARK: - Quit
    @IBAction func quit(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QuitQuiz") as! QuitQuizViewController
        vc.image = self.image
        vc.username = self.username
        self.addChild(vc)
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    //MARK: - Quiz Handling
    func setQuestion(questionIndex:Int){
        if(questionIndex <= amountOfQuestions - 1){
            questionNrLabel.text = "\(currentQuestionIndex + 1)/10"
            categoryLabel.text = quiz[questionIndex].category.htmlAttributedString?.string
            questionLabel.text = quiz[questionIndex].question.htmlAttributedString?.string
            
            let maxButtonIndex: Int = quiz[questionIndex].incorrect_answers.count
            let correctButtonIndex = Int.random(in: 0...maxButtonIndex)
            var allAnswers:[String] = quiz[questionIndex].incorrect_answers
            allAnswers.insert(quiz[questionIndex].correct_answer, at: correctButtonIndex)
            
            if(quiz[questionIndex].type != "multiple"){
                optionButtons[2].isHidden = true
                optionButtons[3].isHidden = true
            }
            else{
                optionButtons[2].isHidden = false
                optionButtons[3].isHidden = false
            }
            
            for n in 0...maxButtonIndex {
                optionButtons[n].setTitle(allAnswers[n].htmlAttributedString?.string, for: UIControl.State.normal)
            }
        }
    }
    
    func optionPressed(optionNr: Int){
        if(oldQuestion != quiz[currentQuestionIndex].question ){
            usersAnswer.append(optionButtons[optionNr - 1].titleLabel?.text)
            if(optionButtons[optionNr - 1].titleLabel?.text == quiz[currentQuestionIndex].correct_answer){
                optionButtons[optionNr - 1].pulsate()
                correctlyAnswered[currentQuestionIndex] = true
                optionButtons[optionNr - 1].backgroundColor = UIColor.green
                MusicPlayer.shared.soundEffect(soundEffect: "Right")        }
            else{
                optionButtons[optionNr - 1].shake()
                correctlyAnswered[currentQuestionIndex] = false
                optionButtons[optionNr - 1].backgroundColor = UIColor.red
                MusicPlayer.shared.soundEffect(soundEffect: "Wrong")
            }
            disableButtons(optionNr: optionNr)
            oldQuestion = quiz[currentQuestionIndex].question
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.currentQuestionIndex += 1
                if(self.currentQuestionIndex == 10){
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "ShowResults", sender: self)
                    }
                }
                self.setQuestion(questionIndex: self.currentQuestionIndex)
                self.resetColor()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.3){
                self.resetButtons()
            }
        }
    }
    
    //MARK: - Label Functions
    func setLabelBorders(){
        for subview in view.subviews {
            if(subview.tag == 0 || subview.tag == 1){
                subview.layer.cornerRadius = 10
            }
        }
    }
    
    //MARK: - Button Functions
    func disableButtons(optionNr: Int){
        for button in optionButtons {
            button.isEnabled = false
            button.setTitleColor(UIColor(red: 1, green: 1, blue: 1, alpha:1), for: UIControl.State.disabled)
        }
    }
    
    func resetButtons(){
        for button in optionButtons {
            button.isEnabled = true
        }
    }
    func resetColor(){
        for button in optionButtons {
            button.backgroundColor = UIColor.black
        }
    }
    @IBAction func option1Pressed(_ sender: Any) {
        optionPressed(optionNr: 1)
    }
    
    @IBAction func option2Pressed(_ sender: Any) {
        optionPressed(optionNr: 2)
    }
    
    @IBAction func option3Pressed(_ sender: Any) {
        optionPressed(optionNr: 3)
    }
    
    @IBAction func option4Pressed(_ sender: Any) {
        optionPressed(optionNr: 4)
    }
    
    func getButtons() -> [UIButton] {
        var btnArray:[UIButton] = []
        for subview in view.subviews {
            if(subview is UIButton && subview.tag != 99) {
                btnArray.append(subview as! UIButton)
            }
        }
        for btn in btnArray{
            btn.layer.cornerRadius = 10
        }
        return btnArray
    }
    
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.destination is ResultViewController
        {
            let vc = segue.destination as? ResultViewController
            vc?.result = correctlyAnswered
            vc?.usersAnswer = usersAnswer
            vc?.quiz = quiz
            vc?.image = self.image
            vc?.username = self.username
        }
    }
}

//MARK: - Class Extensions
extension String {
    var htmlAttributedString: NSAttributedString? {
        return try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    }
}

extension UIButton {
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: "pulse")
    }
  
    func shake() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.2
        shake.repeatCount = 2
        shake.autoreverses = true
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        shake.fromValue = fromValue
        shake.toValue = toValue
        layer.add(shake, forKey: "position")
    }
}
