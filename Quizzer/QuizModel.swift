//
//  QuizModel.swift
//  Quizzer
//
//  Created by Enis Hasanaj on 2019-11-11.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//
import Foundation

//MARK: - Quiz Response
class QuizResponse:Decodable{
    var response_code: Int
    var results: [Questions]
}

//MARK: - Questions
class Questions:Decodable {
    var category: String
    var type: String
    var difficulty: String
    var question: String
    var correct_answer: String
    var incorrect_answers: [String]
}
