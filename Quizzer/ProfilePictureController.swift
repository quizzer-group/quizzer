//
//  ProfilePictureController.swift
//  Quizzer
//
//  Created by Voltair on 2019-11-14.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import UIKit

class ProfilePictureController: UIViewController, ImagePickerDelegate, UITextFieldDelegate {
    @IBOutlet weak var MusicSwitch: UISwitch!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var changeProfilePictureButton: UIButton!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    var profileImage = UIImage(systemName: "person.fill")
    var imagePicker: ImagePicker!
    var userName: String = ""
    
    @IBAction func MusicSwitch(_ sender: UISwitch) {
        if MusicSwitch.isOn{
            MusicPlayer.shared.startBackgroundMusic("song")
        }
        else{
            MusicPlayer.shared.stopBackgroundMusic()
        }
    }
    
    // If user did not choose any username, set a default name else just perform segue.
    @IBAction func Go(_ sender: Any) {
        if username.text == ""{
            self.username.text = "Einstein"
        }
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "ToHome", sender: self)
        }
    }
    

    //MARK: - View Loaded
    override func viewDidLoad() {
        self.navigationItem.setHidesBackButton(true, animated:true);
        MusicPlayer.shared.startBackgroundMusic("song")
        super.viewDidLoad()
        
        self.imagePicker = ImagePicker(self, self)
        changeProfilePictureButton.layer.cornerRadius = 10
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
        self.imageView.image = profileImage
        self.username.text = userName
        configureTextField()
        configureTapGesture()
        
        background.image = UIImage(named: "background_3")
    }
    //MARK: - Namefield
    private func configureTextField(){
        username.delegate = self
    }
    //MARK: - Tap Gesture
    private func configureTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfilePictureController.handleTap))
        view.addGestureRecognizer(tap)
    }
    @objc func handleTap(){
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK - Image picker
    @IBAction func showImagePicker(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    func didSelect(image: UIImage?) {
        self.imageView.image = image
    }
    //MARK: - Prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let destination = segue.destination as? ViewController,
            let image = self.imageView.image {
            destination.image = image
            destination.username = username.text ?? ""
        }
    }
}


