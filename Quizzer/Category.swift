//
//  Category.swift
//  Quizzer
//
//  Created by William Söder on 2019-11-08.
//  Copyright © 2019 TeamQuizzer. All rights reserved.
//

import Foundation
import UIKit

//MARK: Category
class Category {
    let image: UIImage?
    let title: String
    let id: Int
    
    //MARK: - Initialization
    init(image: UIImage?, title: String, id: Int){
        self.image = image
        self.title = title
        self.id = id
    }
}
